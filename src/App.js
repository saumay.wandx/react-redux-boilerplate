import React, {  } from 'react';
import './App.css';
import { Provider } from 'react-redux'
import Main from './Main';

const  App =({store}) =>{
  return (
    <div className="App">
      <Provider store={store}>
        <Main />
      </Provider>
    </div>
  );
}

export default App;
