import React from 'react'
import { colors } from '../../constants/colors'

export default function AddFileButton(props) {
    return (
        <>
        {props.docsArr.length>0 && props.docsArr.map((i)=>{
                            return(
                                <div className="d-flex flex-row justify-content-between align-items-center px-2 mb-2" style={{width:'100%', background: colors.shadywhite, fontSize:10}}>
                                   <span style={{fontSize:10}}> {`${i.name} (${(i.size*0.000001).toFixed(1)}MB)`}</span>
                                    <button className="btn btn-light btn-sm" style={{background:'transparent'}} onClick={()=>props.deleteDocsFn(i)}><i className="fa fa-times" aria-hidden="true" style={{ color: '#000', fontSize:12, opacity:0.5}}></i></button>
                                </div>
                            )
                        })}

        <div className="d-flex " style={{width:'100%', position:'relative'}}>
            <input style={{opacity:0, position:'absolute', top:0, left:0, right:0, bottom:0, zIndex:1}} type="file" multiple className="form-control"  accept=".png,.jpeg,.pdf,.jpg"  onChange={(e)=>props.addDocsFn(e)}/>
            
            <div type="button" className="btn d-flex flex-row align-items-center">
                <span style={{display:'flex',background: colors.dullorange, color: colors.browntext, padding:15, fontSize:10, justifyContent:'center', alignItems:'center', width:17, height:17, borderRadius:5, textAlign:'center', fontWeight:'500', boxShadow:'1px 2px 3px 0 #f1d3b2', marginRight:10}}>+</span>
                <span style={{color: colors.browntext, fontSize:12, fontWeight:'500' }}> Add Attachement</span>
            </div>
            
        </div>
        </>
    )
}
