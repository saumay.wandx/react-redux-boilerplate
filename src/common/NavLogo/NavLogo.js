import React from 'react'
import './NavLogo.css'
import Logo from '../../Layouts/Logo/Logo'
export default function NavLogo() {
    return (
        <div>
            <Logo />
            <div className="px-3 py-1 text-left">Chainflux Technologies Pvt. Ltd.</div>
        </div>
    )
}
