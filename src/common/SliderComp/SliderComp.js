import React, { useEffect } from 'react'
import './SliderComp.css'
import Slider      from 'react-slick';
import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';

import { Link } from 'react-router-dom';
import graph from '../../assets/images/graph.png'
export default function SliderComp() {
    let the_slider;

    // useEffect(() => {
    //     document.getElementById("slider_container").addEventListener('wheel', (e) => {
    //         slide(e.wheelDelta);
    //     })
    // }, [])
    const settings = {
        
        infinite:true,
        speed: 1000,
        initialSlide:0,
        slidesToShow: 3,
        slidesToScroll: 1,
        swipe:true,
        arrows:true,
        pauseOnHover:false, 
        nextArrow: <SampleNextArrow />   ,
        previousArrow: <SamplePrevArrow />,
        variableWidth: true,
        responsive: [
            {
              breakpoint: 1024,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 2,
                infinite: true,
               
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                initialSlide: 2,
              }
            },
            {
              breakpoint: 480,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1
              }
            }
          ]            
      };

     const  slide=(y)=>{
        y > 0 ? (
            the_slider.slickNext()
        ) : (
            the_slider.slickPrev()
        )
    }


      const renderImages =()=>{
        return  [1,2,3,4,5].map((i, index)=>{
            return(
              <div key={index.toString()} style={{marginLeft:10}}>

                <div className="slider_item" key={index} data-index={index} >

                    <div className="item_inner">
                        
                        <div className="item_inner_graph">
                            <img src={graph} width="50px" height="50px"/>
                        </div>

                        <div className="item_inner_data">

                            <div className="item_inner_data_activity">
                                Data Scrapred
                            </div>

                            <div className="item_inner_data_amount">
                                <div className="item_inner_data_quantity">
                                    100
                                </div>
                                <div className="item_inner_data_unit">
                                    tons
                                </div>

                            </div>

                        </div>

                    </div>
                   

                </div>
                  </div>
        )})


    }

    return (
        <div className="slider_wrapper">

            
                  <div className="slider_container animated wow fadeInUp  px-sm-0"  id="slider_container">
                     <Slider {...settings} style={{height:'100%'}} className="slider" ref={slider => the_slider=slider}>
                        {renderImages()}
                    </Slider>
                  </div>
          
          </div>
    )
}

const  SampleNextArrow =(props) =>{
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", right:"-5px" }}
        onClick={onClick}
      ><i className="fa fa-chevron-circle-right" id="slider_next_btn" aria-hidden="true" style={{color:'#815b41', fontSize:30, position:'absolute', right:0}}> </i>
          </div>
    );
  }
  
const  SamplePrevArrow =(props) =>{
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", right:"0px" }}
        onClick={onClick}
      ><i className="fa fa-chevron-circle-up" id="slider_next_btn" aria-hidden="true" style={{color:'#444'}}> </i>
          </div>
    );
  }
  