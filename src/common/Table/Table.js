import React, {useEffect, useState} from 'react'
import './Table.css';
import { convertTime } from '../../constants/common';

import AddBatchModal from '../../comps/AddBatchModal/AddBatchModal';


export default function Table(props) {

    const [activeItem, setActiveItem] = useState('');
    useEffect(() => {
        console.log(props.activeItem);
        console.log(props.siteList);

        console.log("table")

    }, [])

    function renderHeads(tableHeads) {
        return tableHeads.map(i => {
            return (
                <th key={i}>{i}</th>
            )
        })
    }

    function renderStatus(type, status){
        switch(type){
            case 'samplingStatus':
                if(status === 0){
                    return <span style={{color:'#9d9d9d'}}>Not Initiated</span>
                }else if(status === 1){
                    return <span style={{color:'#f7b500'}}>In-Progress</span>
                }else if( status === 2){
                    return <span style={{color:'#41987b'}}>Complete</span>
                }else if(status === 3){
                    return <span style={{color:'red'}}>Rejected</span>
                }else{
                    return '--'
                }
            case 'validationCheck':
                if(status === 0){
                    return <span style={{color:'#f7b500'}}>Pending</span>
                }else if(status  === 1){
                    return <span style={{color:'#41987b'}}>Complete</span>
                }else if(status === 2){
                    return <span style={{color:'red'}}>Failed</span>
                }else{
                    return '--'
                }
            default:
                return null
        }
    }

    function renderRows(siteList) {
        //console.group(siteList)
        console.log(props.siteList);
        return siteList.map(i => {
            if(props.type === 'all'){
                const {
                    _id,
                    createdAt,
                    samplingStatus,
                    totalBatches,
                    currentStatus,
                    validationCheck,

                } = i;
                console.log(i)
                 
                return (
                    <tr className={`${props.type} ${activeItem === _id ? "active" : ''}`}key={_id} onClick={() => {
                        props.handleClick ? props.handleClick(true) : console.log("no handdler");
                        props.setItem ? props.setItem(_id) : console.log("no handdler");
                        setActiveItem(_id)
                    }}>
                        <td>
                            <div className="radiotext">
                                <label for={_id}>{_id}</label>
                            </div>
                        </td>

                        <td>
                            <div className="radiotext">
                                <label for={createdAt}>{convertTime(createdAt)}</label>
                            </div>
                        </td>
                        <td>
                            <div className="radiotext">
                                <label for={totalBatches}>{i.batch?.length || '--'}</label>
                            </div>
                        </td>

                        <td>
                            <div className="radiotext">
                                <label for={samplingStatus}>{renderStatus('samplingStatus', samplingStatus)}</label>
                            </div>
                        </td>
                        <td>
                            <div className="radiotext">
                                <label for={validationCheck}>{renderStatus('validationCheck',validationCheck)}</label>
                            </div>
                        </td>
                        <td>
                            <div className="radiotext">
                                <label for={currentStatus}> {'Doing some stuff'}</label>
                                {activeItem === _id && <AddBatchModal activeItem={activeItem}/>}
                            </div>
                        </td>
                        
                    </tr>
                )
             }
            if(props.type === 'records'){
                const {
                    _id,
                    createdAt,
                    samplingStatus,
                    totalBatches,
                    currentStatus,
                    validationCheck,

                } = i;
                console.log(i)
                 
                return (
                    <tr className={`${props.type} ${activeItem === _id ? "active" : ''}`}key={_id} onClick={() => {
                        props.handleClick ? props.handleClick(true) : console.log("no handdler");
                        props.setItem ? props.setItem(_id) : console.log("no handdler");
                        setActiveItem(_id)
                    }}>
                        <td>
                            <div className="radiotext">
                                <label for={_id}>{_id}</label>
                            </div>
                        </td>

                        <td>
                            <div className="radiotext">
                                <label for={createdAt}>{convertTime(createdAt)}</label>
                            </div>
                        </td>
                        <td>
                            <div className="radiotext">
                                <label for={totalBatches}>{i.batch?.length || '--'}</label>
                            </div>
                        </td>
                        <td>
                            <div className="radiotext">
                                <label for={totalBatches}>{i.batch?.bars?.length || '--'}</label>
                            </div>
                        </td>

                        <td>
                            <div className="radiotext">
                                <label for={samplingStatus}>{renderStatus('samplingStatus', samplingStatus)}</label>
                            </div>
                        </td>
                        <td>
                            <div className="radiotext">
                                <label for={validationCheck}>{renderStatus('validationCheck',validationCheck)}</label>
                            </div>
                        </td>
                        <td>
                            <div className="radiotext">
                                <label for={currentStatus}> {'Doing some stuff'}</label>
                                {activeItem === _id && <AddBatchModal activeItem={activeItem}/>}
                            </div>
                        </td>
                        
                    </tr>
                )
             }
        })

    }


    return (
        <div id="update_table" style={{ height: props.height || 525}}>
            <table className="table text-left">
                <thead>
                <tr>

                    {renderHeads(props.tableHeads)}

                </tr>
                </thead>

                <tbody>


                {props.siteList.length > 0 ? renderRows(props.siteList) : null}


                </tbody>


            </table>
        </div>
    )


}


