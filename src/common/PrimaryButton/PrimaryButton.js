import React from 'react'

export default function PrimaryButton(props) {
    return (
        <div style={{display:'flex', justifyContent:'flex-end', alignItems:'flex-end'}}>
            <button type="button" style={{backgroundColor:'#eb902d', color:'#fff', padding:props.padding ? props.padding :'11px 20px', borderTopLeftRadius:8, opacity: props.disabled ? 0.5 :1}} disabled={props.disabled} onClick={()=> props.onClick ?props.onClick(): alert('coming soon')}>
                {props.label}
            </button>
        </div>
    )
}
