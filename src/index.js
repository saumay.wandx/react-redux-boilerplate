import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
 
import * as serviceWorker from './serviceWorker';
import { createStore } from 'redux'
import rootReducer from './redux/reducers/'
import App from './App';
import store from './redux/store'

ReactDOM.render(
 <App store={store}/>,
  document.getElementById('root')
);

serviceWorker.unregister();
