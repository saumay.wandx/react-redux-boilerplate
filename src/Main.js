import React, { useState, useEffect } from 'react'
import Routes from './Routes'
import { getLocalItem } from './constants/storage'
import { useSelector, useDispatch } from 'react-redux'
import { loginAction } from './redux/actions/auth.action'

const Main =() =>{
    const dipatch =useDispatch()
    const [user, setUser] = useState(false)
    let userData = useSelector(state=>state.auth)
    useEffect(() => {
        console.log('main')
        checkData()
    },[])

    const checkData = () =>{
        let localloggedIn = getLocalItem('loggedIn');
        if(localloggedIn){
            console.log(localloggedIn)
            setUser(localloggedIn)
            dipatch(loginAction(localloggedIn))
        }else{
            setUser(false)
        }
    }
    return (
        <Routes userData={user} />
    )
}

export default Main;