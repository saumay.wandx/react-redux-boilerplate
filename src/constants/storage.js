export const setLocalItem = (key, value) => {
 
    (localStorage.setItem(key, JSON.stringify(value)))
    return true
}

export const getLocalItem = (key) =>{
    
    let item = (localStorage.getItem(key))
    return (JSON.parse(item))
   
}

export const removeItem = (key) =>{
    return new Promise((resolve, reject)=>{
        if(!key || typeof key !== 'string'){reject('provide proper key type')}
        resolve(localStorage.removeItem(key))
    })
}

export const removeAllItem =()=>{
    localStorage.clear();
}

