import $ from 'jquery';
export const  limitTitle = (title, limit =50) => {
    const newTitle = [];
    if (title.length > limit) {
        title.split(' ').reduce((acc, cur) => {
            if (acc + cur.length <= limit) {
                newTitle.push(cur);
            }
            return acc + cur.length;
        }, 0);

        // return the result
        return `${newTitle.join(' ')} ...`;
    }
    return title;
}

export const convertTime = (time) => {
    const date = new Date(`${time}`);
    let year = date.getFullYear();
    let month = date.getMonth() + 1;
    let dt = date.getDate();

    if (dt < 10) {
        dt = '0' + dt;
    }
    if (month < 10) {
        month = '0' + month;
    }




    const converted = dt + '/' + month + '/' + year;
    return converted
}

export const hideModal = () =>{
    $('#exampleModalCenter').css("display","none") 
    $('.modal-backdrop').remove()
    $('.modal-open').removeClass("modal-open")
}
