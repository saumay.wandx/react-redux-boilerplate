import { SETLOGO } from "../types"

 
const INIT_STATE ={logo:false, tagLine:'ABC XYZ Pvt Ltd'}
const views = (state = INIT_STATE, action) => {
    switch (action.type) {
      case SETLOGO:
        state = {
            ...state,
            logo: action.payload,
        }
        break
      case SETTAG:
         state={
             ...state,
             tagLine: action.payload
         }
        break
      default:
        return state
    }
    return state;
  }

  export default views;