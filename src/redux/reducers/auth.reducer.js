import { setLocalItem, removeItem } from "../../constants/storage"
import { LOGIN, LOGOUT } from "../types"

const INIT_STATE ={loggedIn:false, data:null}
const auth = (state = INIT_STATE, action) => {
    switch (action.type) {
      case LOGIN:
        state = {
            ...state,
            loggedIn: true,
            data: action.payload
        }
        break
      case LOGOUT:
        removeItem('loggedIn')
        state={... state, loggedIn:false, data:null}
        break
      default:
        return state
    }
    return state;
  }

  export default auth;