import { combineReducers } from 'redux'
 
import auth from './auth.reducer'
import views from './views.reducer'
export default combineReducers({
  auth,views
})