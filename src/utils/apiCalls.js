import { fetchApi } from "../constants/api";
import { LOGIN } from "./constants";

export const loginCall = async (payload)=>{
    return new Promise ( async (resolve, reject)=>{    
        if(!payload) reject({msg: "No payload"})

        let res = await fetchApi(LOGIN, "POST", payload, 200);
        console.log(res)
        if(res?.responseBody?.status){
            resolve({status: true , data:res.responseBody.User[0]})
        }else{
            resolve({status: false})
        }
    })
}