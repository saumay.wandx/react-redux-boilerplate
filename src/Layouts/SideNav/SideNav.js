import React from 'react'
import './Sidenav.css'
import NavLogo from '../../common/NavLogo/NavLogo'
import { NavLink } from 'react-router-dom'
export default function SideNav(props) {
    return (
        <div id="mySidenav" className="sidenav">
            <NavLogo />
            {/* <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a> */}
            <div className="py-5 create_record_div d-flex flex-row align-items-center" >
                <a  style={{fontSize:14, fontWeight:'500',opacity:1,}} data-toggle="modal" data-target="#exampleModalCenter"> <span id="plus_btn">+</span> Create New Record</a>
            </div>
           <div style={{padding:'20px 0px'}}>
                <NavLink exact activeClassName="active" to="/" className="nav-link"><span className="navIcons"><i className="fa fa-area-chart" aria-hidden="true" style={{color:'#eb902d', fontSize:21}}></i></span> Overview</NavLink>
                <NavLink exact activeClassName="active" to="/records" className="nav-link"> <span className="navIcons"><i className="fa fa-file" aria-hidden="true" style={{color:'#eb902d', fontSize:21}}></i></span>Records</NavLink>
                <NavLink exact activeClassName="active" to="/manage" className="nav-link"><span className="navIcons"><i className="fa fa-users" aria-hidden="true" style={{color:'#eb902d', fontSize:21}}></i></span> Manage</NavLink>
           </div>
           <div style={{paddingTop:'120px', justifyContent:'flex-end'}}>
                
                <NavLink exact activeClassName="active" to="/settings" className="nav-link"><span className="navIcons"><i className="fa fa-cog" aria-hidden="true" style={{color:'#eb902d', fontSize:21}}></i></span> Settings</NavLink>
           </div>
        </div>
    )
}
