import React from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import DashboardIcon from '@material-ui/icons/Dashboard';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import PeopleIcon from '@material-ui/icons/People';
import SettingsIcon from '@material-ui/icons/Settings';
import { NavLink } from 'react-router-dom';
import store from '../../redux/store';
import CreateButton from './CreateButton';
import { colors } from '../../constants/colors';
import './listItems.css'

export const mainListItems = ()=>{
    const state = store.getState();
    return(
        <div className="py-5">
          <ListItem>
            <div className="pb-3">
                {state.views.logo ? <img src={state.views.logo} style={{width:120}}/> : 'Logo'}
            </div>
          </ListItem>

          <ListItem>
            <div className="pb-5">
                {state.views.tagline}
            </div>
          </ListItem>

          <CreateButton />
          <div className="position-absolute" style={{height:1, background: colors.potBrown, bottom:0, left:10, right:10}} />

        </div>
      )
}

export const secondaryListItems = (
  <div className="pb-5 pt-3">
   
    <ListItem button component={NavLink} to="/dashboard" activeClassName="active"  className="nav-link">
        <DashboardIcon style={{color:'#eb902d', margin:'0 5px'}} />
        <ListItemText disableTypography primary="Overview"  />
    </ListItem>

    <ListItem button component={NavLink} to="/records" activeClassName="active"  className="nav-link">
        <InsertDriveFileIcon style={{color:'#eb902d', margin:'0 5px'}}/>
        <ListItemText disableTypography primary="Records"  />
    </ListItem>
    
    <ListItem button  component={NavLink} to="/manage" activeClassName="active"  className="nav-link">
        <PeopleIcon style={{color:'#eb902d', margin:'0 5px'}}/>
        <ListItemText disableTypography primary="Manage"/>
    </ListItem>
  </div>
);

export const thirdListItems = (
  <div className="d-flex pb-3 align-self-end" style={{width:'100%'}}>
   
    <ListItem button component={NavLink} to={{ pathname: '/settings'}} activeClassName="active" className="nav-link" style={{marginTop:90}} >
      <SettingsIcon style={{color:'#eb902d', margin:'0 5px',}} />
      <ListItemText disableTypography primary="Settings"  />
    </ListItem>
     
  </div>
);