import React from 'react'
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

const CreateButton = () => {
    return (
        <ListItem button data-toggle="modal" data-target="#exampleModalCenter">

        <div 
            style={{
                background: '#eb902d', 
                padding: '2px 9px', borderRadius: 8,
                boxShadow: '1px 3px 4px 0 #f1d3b2, inset 1px 1px 2px 0 rgba(255, 255, 255, 0.33)', 
                marginRight: 4}}
                >+</div>
            <div style={{fontWeight:'500'}}>
                Create New Record
            </div>
        </ListItem>
     
    )
}

export default CreateButton
