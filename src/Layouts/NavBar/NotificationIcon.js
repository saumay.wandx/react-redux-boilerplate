import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { logoutAction } from '../../redux/actions/auth.action'
import './NotificationIcon.css'

const NotificationIcon = () => {

    const dispatch = useDispatch()
    const auth = useSelector(state=>state.auth)
    console.log(auth)
    return (
        <div className="notification-menu">
                    <div class="dropdown show">
                        <div className="d-flex flex-row justify-content-around align-items-center" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <div className="notification-icon">
                                <span className="position-absolute badge notificationIconBadge">
                                    {auth?.data?.role}
                                </span>
                                <i class="fa fa-bell" aria-hidden="true" style={{fontSize:20, color:'#eddfd5'}}></i>
                            </div>
                        </div>
                        <ul class="dropdown-menu nseMenuDropdown p-0" aria-labelledby="dropdownMenuLink">
                            <li className="nseMenuDropdown-item" onClick={e=>{ dispatch(logoutAction())}}>Some Action</li> 
                            <li className="nseMenuDropdown-item" onClick={e=>{ dispatch(logoutAction())}}>Some Action</li> 
                            <li className="nseMenuDropdown-item" onClick={e=>{ dispatch(logoutAction())}}>Some Action</li> 
                            <li className="nseMenuDropdown-item" onClick={e=>{ dispatch(logoutAction())}}>Some Action</li> 
                        </ul>
                    </div>
                </div>
    )
}

export default NotificationIcon
