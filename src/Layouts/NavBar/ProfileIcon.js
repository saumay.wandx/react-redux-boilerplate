import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { logoutAction } from '../../redux/actions/auth.action'
import './ProfileIcon.css'
import { colors } from '../../constants/colors'

const ProfileIcon = () => {

    const dispatch = useDispatch()
    const auth = useSelector(state=>state.auth)
    console.log(auth)
    return (
        <div>
                    <div class="dropdown show">
                        <div className="d-flex flex-row justify-content-around align-items-center" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                           
                            <div className="px-4">
                                {auth?.data?.role}
                            </div>

                            <div className="profile-icon">
                                <i class="fa fa-user" aria-hidden="true" style={{fontSize:20, color:colors.potBrown}}></i>
                            </div>
                        </div>

                        <ul class="dropdown-menu nseMenuDropdown p-0" aria-labelledby="dropdownMenuLink">
                            <li className="nseMenuDropdown-item" onClick={e=>{ dispatch(logoutAction())}}>Logout</li> 
                        </ul>
                    </div>
                </div>
    )
}

export default ProfileIcon
