import React from 'react'
import './NavBar.css'
import { logoutAction } from '../../redux/actions/auth.action'
import { useDispatch, useSelector } from 'react-redux'
import { NavLink } from 'react-router-dom'
import NavLogo from '../../common/NavLogo/NavLogo'
import CreateNewModal from '../../comps/CreateNewModal/CreateNewModal'
import ProfileIcon from './ProfileIcon'
import NotificationIcon from './NotificationIcon'
import SearchBar from '../../comps/SearchBar/SearchBar'
export default function NavBar() {
    const dispatch = useDispatch()
    const auth = useSelector(state=>state.auth)
    return (
         <>
            <nav className="d-flex flex-row navbar navbar-light bg-light">
                <div className="py-4">
                    <SearchBar color="#fefe32" />
                </div>
                <div className="d-flex flex-row align-item-around">
                    <NotificationIcon />
                        <ProfileIcon />
                </div>
            </nav>


            {/* SIDE NAV STARTS */}

            <div id="mySidenav" className="sidenav">
                <NavLogo />
                {/* <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a> */}
                <div className="py-5 create_record_div d-flex flex-row align-items-center" >
                    <a  style={{fontSize:14, fontWeight:'500',opacity:1,}} data-toggle="modal" data-target="#exampleModalCenter"> <span id="plus_btn">+</span> Create New Record</a>
                </div>
                <div style={{padding:'20px 0px'}}>
                    <NavLink exact activeClassName="active" to="/dashboard" className="nav-link"><span className="navIcons"><i className="fa fa-area-chart" aria-hidden="true" style={{color:'#eb902d', fontSize:21}}></i></span> Overview</NavLink>
                    <NavLink exact activeClassName="active" to="/records" className="nav-link"> <span className="navIcons"><i className="fa fa-file" aria-hidden="true" style={{color:'#eb902d', fontSize:21}}></i></span>Records</NavLink>
                    <NavLink exact activeClassName="active" to="/manage" className="nav-link"><span className="navIcons"><i className="fa fa-users" aria-hidden="true" style={{color:'#eb902d', fontSize:21}}></i></span> Manage</NavLink>
                </div>
                <div style={{paddingTop:'120px', justifyContent:'flex-end'}}>
                    <NavLink exact activeClassName="active" to="/settings" className="nav-link"><span className="navIcons"><i className="fa fa-cog" aria-hidden="true" style={{color:'#eb902d', fontSize:21}}></i></span> Settings</NavLink>
                </div>
            </div>
            <CreateNewModal/>
</>
        
    )
}
