import React from 'react'
import {
    BrowserRouter as Router,
    Route,  
    Switch,
    BrowserRouter,
    Redirect,
    useHistory,
    useLocation} from "react-router-dom";
import LoginForm from './pages/LoginForm/LoginForm';
import Dashboard from './pages/Dashboard/Dashboard';
import MenuBar from './Layouts/MenuBar/MenuBar';
import Records from './pages/Records/Records';
import Manage from './pages/Manage/Manage';
import { useSelector } from 'react-redux';
import NavBar from './Layouts/NavBar/NavBar';

const Routes = (props) =>{
    console.log(props)
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/(login)" render={()=> <LoginContainer {...props}/>}/>
                <Route render={(props2)=><DefaultContainer {...props} {...props2}  />}/>
                <Route render={()=> <LoginContainer {...props}/>} />
            </Switch>
        </BrowserRouter>
    )
}

const LoginContainer = (props) => {
    console.log(props)
    let history = useHistory();
    let location = useLocation();

    let { from } = location.state || { from: { pathname: "/" } };

    const isLoggedIn = useSelector(state=> state.auth.loggedIn)

    return (
    <div>
      <Route exact path="/" render={() => <Redirect to={`${isLoggedIn ? from :'/login'}`} />} />
      <Route path="/login" render={()=> !props.userData ? <LoginForm /> : <Redirect to='/dashboard' />} />
   
    </div>
  )}

const DefaultContainer = (props) => {
    console.log(props)
    return(
     

     
                    <div>
                        <NavBar />
                        <Route exact path="/" render={() => <Redirect to={`${props.userData ? '/dashboard':'/login'}`} />} />
                         
                        <ProtectedRoute
                            userData={props.userData}
                            path="/dashboard"
                            component={Dashboard}
                        />

                        <ProtectedRoute
                            userData={props.userData}
                            path="/records"
                            component={Records}
                        />
                        <ProtectedRoute
                            userData={props.userData}
                            path="/manage"
                            component={Manage}
                        />
                        
                    </div>
              
   
 )}


 const PublicRoute = ({ userData, ...props }) => {
    const isAllowed = userData
    return isAllowed
        ? <Redirect to="/login" />
        : <Route {...props} />;
};

 const ProtectedRoute =  ({ userData, ...props }) => {
    const isAllowed = userData
    return isAllowed
        ? <Route {...props} />
        : <Redirect to={{pathname:"/login", state: { from: props.path } }} />;
};

export default Routes