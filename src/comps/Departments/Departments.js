import React from 'react'
import './Departments.css'
import PrimaryButton from '../../common/PrimaryButton/PrimaryButton'

export default function Departments() {



    return (
        <div className="departments" style={{backgroundColor:'#fff', borderRadius:16, boxShadow:'0 4px 10px #eddfd5',overflow:'hidden'}}>
            <div style={{flex:1, display:'flex', justifyContent:'flex-start', padding:15, fontSize:14}}>
                Departments
            </div>

            <ul className="activities" style={{height:372-27, overflow:'scroll'}}>
                {[1,2,2,2,2,2,2,2,2,2,2,2,2,,2,2,2,2,2,].map((item,index)=><Department key={index.toString()}/>)}
            </ul>

           <PrimaryButton label="Add Department"/>
        </div>
    )
}


const Department = (props) => {
    return(
        <li>
            <div style={{display:'flex', flexDirection:'row', padding:'10px 10px'}}>
                <div style={{display:'flex', padding:'0px 5px'}}>
                    <div style={{padding:10, borderRadius:10, width:32, height:32, boxShadow:'0 4px 10px 0 #eddfd5', display:'flex', justifyContent:'center', alignItems:'center'}}>
                        <i className="fa fa-user" aria-hidden="true" style={{fontSize:21, color:'#eddfd5'}}></i>
                    </div>
                </div>

                <div style={{display:'flex', flexDirection:'column', padding:'0px 5px', justifyContent:'start'}}>

                    <div style={{fontSize:10, display:'flex', fontWeight:'500'}}>
                        Saumay Paul
                    </div>
                    <div style={{fontSize:12, lineHeight:1.5, display:'flex', paddingTop:5}}>
                        Manager, Sourcing Department
                    </div>

                </div>
            </div>
        </li>
    )
}