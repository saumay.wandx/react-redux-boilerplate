import $ from 'jquery'; 
import React, { useState, useEffect } from 'react'
import PrimaryButton from '../../common/PrimaryButton/PrimaryButton'
import './CreateNewModal.css'
import { render } from '@testing-library/react'
import { colors } from '../../constants/colors'
import AddFileButton from '../../common/AddFileButton/AddFileButton';
import success_img from '../../assets/images/illustrations/success.png'
import { hideModal, limitTitle } from '../../constants/common';

export default function CreateNewModal() {

    const [goldType, setGoldType] = useState(null)
    
/* --------------------------------- Type 1 --------------------------------- */
    const [step, setStep] = useState(1)
    const [fullname,setFullName] =useState('')
    const [address,setAddress] =useState('')
    const [countryOrigin,setCountryOrigin] =useState('')
    const [shipperName,setShipperName] =useState('')
    const [minerDocs,setMinerDocs] =useState([])
    const [originProofDocs,setOriginProofDocs] =useState([])
    const [uin, setUIN] = useState('')


/* --------------------------------- Type 2 --------------------------------- */
    const [supplierName, setSupplier] = useState('')
    const [supplierAddress, setSupplierAddress] = useState('')
    const [sourcingDate, setSourcingDate] = useState(new Date)
    const [kyc, setKyc] = useState('no')
    const [aadhar, setAadhar] = useState([])
    const [pan, setPan] = useState([])
    const [gst, setgst] = useState([])
    const [roc, setRoc] = useState([])
    const [bank, setBank] = useState([])


/* --------------------------------- Methods -------------------------------- */

    useEffect(() => {
        setStep(1);
        setGoldType(null)
    }, [])

    const moveSteps = async () => {
    //    step !== 4 ? setStep(step+1) : window.location.reload()

        if(goldType === 1){
            switch(step){
                case 1:
                    setStep(2)
                    break
                case 2:
                    setStep(3)
                    break
                case 3:
                    let payload = {
                        rowType:'dore',
                        minerFullname: fullname,
                        minerAddress: address,
                        minerAttachment: '',
                        importCountry: countryOrigin,
                        importOriginProof:'',
                        importPartyName: shipperName,
                    }
                    // createOrder1(payload).then(res=>{ 
                    //     if(res){
                    //         console.log(res)
                    //         setUIN(res.data._id)
                    //         setStep(4)
                    //     }
                    // })
                    break
                case 4:
                    window.location.reload()
                    break
                default:
                    return false
            }
        }else{

            switch(step){
                case 1:
                    setStep(2)
                    break
                case 2:
                    setStep(3)
                    break
                case 3:
                    let payload = {
                        rowType:'scrap',
                        sourceSupplierName: supplierName,
                        sourceDate: sourcingDate,
                        sourceSupplierAddress: supplierAddress,
                        kycDone:true,
                        kycDocuments:[],
                    }
                    // createOrder2(payload).then(res=>{ 
                    //     if(res){
                    //         console.log(res)
                    //         setUIN(res.data._id)
                    //         setStep(4)
                    //     }
                    // })
                    break
                case 4:
                    window.location.reload()
                    break
                default:
                    return false
            }


        }
    }

    const moveBack= () =>{
        setStep(step-1)
    }

    const addMinorDocs = (e) => {
        var filesArr = Array.prototype.slice.call(e.target.files);
        console.log(filesArr)
        if(step ===2){
            setMinerDocs(filesArr.concat(minerDocs))
        }else if(step === 3){
            setOriginProofDocs (filesArr.concat(originProofDocs))
        }
        
    }
    const addFlow2Docs= (e, type) => {
        var filesArr = Array.prototype.slice.call(e.target.files);
        //console.log(filesArr)
        switch(type){
            case 'aadhar':
                setAadhar(filesArr.concat(aadhar))
                break;
            case 'pan':
                setPan(filesArr.concat(pan))
                break;
            case 'gst':
                setgst(filesArr.concat(gst))
                break;
            case 'bank':
                setBank(filesArr.concat(bank))
                break;
            case 'roc':
                setRoc(filesArr.concat(roc))
                break;
            default:
                return false
        }
        
    }

    const deleteFile = (fileItem, filesArr) => {
        let newArr=filesArr.filter(i => i.name !== fileItem.name);
        if(goldType === 1){
            if(step === 2){    
                setMinerDocs(newArr)
            }else if(step ===3){
                setOriginProofDocs(newArr)
            }
        }
    }
    const deleteFlow2File = (fileItem, filesArr, type) => {
        let newArr=filesArr.filter(i => i.name !== fileItem.name);
        switch(type){
            case 'aadhar':
                setAadhar(newArr)
                break;
            case 'pan':
                setPan(newArr)
                break;
            case 'gst':
                setgst(newArr)
                break;
            case 'bank':
                setBank(newArr)
                break;
            case 'roc':
                setRoc(newArr)
                break;
            default:
                return false
        }
    }

    const AttachBtnInline = (props) =>{
        return (
            <div className={`d-flex align-items-center ${props.disabled ? 'invisible': 'visible'}`} style={{ position:'relative', flex:2}}>
                <div className="d-flex" style={{flex:1}}>

                <input style={{opacity:0, position:'absolute', top:0, left:0, right:0, bottom:0, zIndex:1}} type="file" className="form-control"  accept=".png,.jpeg,.pdf,.jpg"  onChange={(e)=>props.addDocsFn(e, props.type)}/>
                
                <div type="button" className="btn d-flex flex-row align-items-center">
                    <span style={{display:'flex',background: colors.dullorange, color: colors.browntext, padding:15, fontSize:10, justifyContent:'center', alignItems:'center', width:17, height:17, borderRadius:5, textAlign:'center', fontWeight:'500', boxShadow:'1px 2px 3px 0 #f1d3b2', marginRight:10}}>+</span>
                    <span style={{color: colors.browntext, fontSize:12, fontWeight:'500' }}> Attach</span>
                </div>
                </div>

              { props.docsArr.length>0 && props.docsArr.map(i=>{
                  return(
                    <div className="d-flex flex-row justify-content-between align-items-center px-2" style={{flex:1,width:'100%', background: colors.shadywhite, fontSize:10}}>
                    <span style={{fontSize:10}}> {`${limitTitle(i.name, 17)} (${(i.size*0.000001).toFixed(1)}MB)`}</span>
                    <button className="btn btn-light btn-sm" style={{background:'transparent'}} onClick={()=>props.deleteDocsFn(i,props.docsArr, props.type)}><i className="fa fa-times" aria-hidden="true" style={{ color: '#000', fontSize:12, opacity:0.5}}></i></button>
                </div>
                  )
              })}
                
            </div>
        )
    }

    const isDisabled = () =>{
        if(goldType === 1){
            switch(step){
                case 1:
                    return !(goldType)
                case 2:
                    return !(fullname.trim().length>0 && address.trim().length>0)
                case 3:
                    return !(countryOrigin.trim().length>0 && shipperName.trim().length>0)
                case 4:
                    return false
                default:
                    return true
            }
        }else{
            switch(step){
                case 1:
                    return !(goldType)
                case 2:
                    return !(supplierAddress.trim().length>0 && supplierName.trim().length>0)
                case 3:
                    return kyc === 'no'
                case 4:
                    return false
                default:
                    return true
            }
        }   

    }

    const GoldType = (props)=>{
        return(
            <div className="d-flex text-center p-3 flex-column justify-content-center align-items-center" onClick={()=>props.onClick(props.type)}>
                <div className={`d-flex justify-content-center align-items-center mb-2 goldType ${goldType === props.type? 'active' : ''}`} style={{overflow:'hidden', backgroundImage:`url(${props.type === 1 ? 'https://goldsalesuganda.net/uploads/s/2/v/z/2vzlgemf047c/img/jEBFPOOg.jpg':'https://thumbs.dreamstime.com/z/scrap-gold-23211134.jpg'})`, backgroundRepeat:'no-repeat', backgroundSize:'cover', boxShadow:'0 10px 20px 0 #bba292'}}>
                    {/* {goldType === props.type && <i className="fa fa-check" aria-hidden="true" style={{color: colors.dullorange, fontSize:48,}}></i>} */}
                   {/* <img src={`${props.type === 1 ? 'https://goldsalesuganda.net/uploads/s/2/v/z/2vzlgemf047c/img/jEBFPOOg.jpg' :'https://thumbs.dreamstime.com/z/scrap-gold-23211134.jpg'}`} style={{width:'100%', height:'100%'}} /> */}
                </div>
                <span style={{fontSize:14, color:'#bba292', marginTop:20, fontWeight:'500'}}>{props.label}</span>
            </div>
        )
    }

    const stepOne =() =>{
        return(
            <>
        <div className="modal-body text-center p-5">
        <h6 style={{color:'#473122'}}>Select raw gold type </h6>

            <div style={{display:'flex', flexDirection:'row', justifyContent:'center', alignItems:'center'}}>
                
                <GoldType label="Imported Dore Gold" onClick={(bool)=>setGoldType(bool)}  type={1}/>
                <GoldType label="Sourced Scrap Gold" onClick={(bool)=>setGoldType(bool)} type={2}/>
            </div>
        </div>
        
        </>
    )
    }
    const stepTwoFlow1 =() =>{
        return(
            <>
        <div className="modal-body text-center p-5">
            <h6 style={{color:'#473122'}}>Provide Miner Details</h6>

            <div style={{display:'flex',  justifyContent:'center', alignItems:'center'}}>
                
                <div className="form-group fuj_form_group pt-4 md-6 modalForm"> 
                    <input type="text"  placeholder="Full Name" className="form-control mb-4" value={fullname} onChange={(e)=>setFullName(e.target.value)} id="fullname"/>
                    <textarea rows="5" type="text"  placeholder="Address" className="form-control mb-4" value={address} onChange={(e)=>setAddress(e.target.value)} id="address"/>

                    <AddFileButton label="Attach Documents" docsArr={minerDocs} addDocsFn={(e)=>addMinorDocs(e)} deleteDocsFn={(i)=>deleteFile(i, minerDocs)} />
                    
                </div>
            </div>
        </div>
        
        </>
    )}

    const stepThreeFlow1 =() =>{
        return(
            <>
        <div className="modal-body text-center p-5">
            <h6 style={{color:'#473122'}}>Provide Import Details</h6>

            <div style={{display:'flex',  justifyContent:'center', alignItems:'center'}}>
                
                <div className="form-group fuj_form_group pt-4 md-6 modalForm"> 
                    <input type="text"  placeholder="Country of Origin" className="form-control mb-4" value={countryOrigin} onChange={(e)=>setCountryOrigin(e.target.value)} id="fullname"/>

                    <AddFileButton label="Attach Origin Proofs" docsArr={originProofDocs} addDocsFn={(e)=>addMinorDocs(e)} deleteDocsFn={(i)=>deleteFile(i, originProofDocs)} />

                    <textarea rows="5" type="text"  placeholder="Shipper/3rd Party Name" className="form-control mb-4" value={shipperName} onChange={(e)=>setShipperName(e.target.value)} id="address"/>
                    
                </div>
            </div>
        </div>
        
        </>
    )}


    const stepTwoFlow2 =() =>{
        return(
            <>
        <div className="modal-body text-center p-5">
            <h6 style={{color:'#473122'}}>Sourcing Details</h6>

            <div style={{display:'flex',  justifyContent:'center', alignItems:'center'}}>
                
                <div className="form-group fuj_form_group pt-4 md-6 modalForm" style={{width:'70%'}}> 
                   <div className="d-flex flex-row align-items-center justify-content-center" style={{width:'100%'}}>
                        <div className="col-md-6 pr-1 pl-0">
                                <input type="text"  placeholder="Aggregator / Supplier Name" className="form-control mb-4" value={supplierName} onChange={(e)=>setSupplier(e.target.value)} id="suppliername"/>
                        </div>
                        <div className="col-md-6 pl-1 pr-0">
                                <input type="date"  placeholder="Sourcing Date" className="form-control mb-4" value={sourcingDate} onChange={(e)=>setSourcingDate(e.target.value)} id="suppliername"/>
                        </div>
                        
                   </div>
                    <textarea rows="5" type="text"  placeholder="Address" className="form-control mb-4" value={supplierAddress} onChange={(e)=>setSupplierAddress(e.target.value)} id="address"/>

                    {/* <AddFileButton label="Attach Documents" docsArr={minerDocs} addDocsFn={(e)=>addMinorDocs(e)} deleteDocsFn={(i)=>deleteFile(i, minerDocs)} /> */}
                    
                </div>
            </div>
        </div>
        
        </>
    )}
    const stepThreeFlow2 =() =>{
        return(
            <>
        <div className="modal-body text-center p-5">
            <h6 style={{color:'#473122'}}>KYC Details</h6>

            <div style={{display:'flex',  justifyContent:'center', alignItems:'center'}}>
                
                <div className="form-group fuj_form_group pt-4 md-6 modalForm" style={{width:'80%'}}> 
                   <div className="d-flex flex-row justify-content-between align-self-center" style={{width:'100%'}}>

                    <div className="d-flex mr-2 ">
                        <span>Is KYC done for aggregator / supplier?</span>
                    </div>
                    <div className="d-flex ">
                        <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" checked={kyc === 'yes'} onChange={(e)=> setKyc(e.target.value)} name="inlineRadioOptions" id="inlineRadio1" value="yes"/>
                                <label class="form-check-label" for="inlineRadio1">Yes</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input"  checked={kyc === 'no'} type="radio" name="inlineRadioOptions" id="inlineRadio2" value="no" onChange={(e)=> setKyc(e.target.value)}/>
                            <label class="form-check-label" for="inlineRadio2">No</label>
                        </div>
                         
                    </div>

                   </div>

                   <div className="d-flex flex-column justify-content-between align-self-center pt-5" style={{width:'100%'}}>
                       <div className="d-flex align-self-start">Select and attach KYC documents </div>

                       <div className="d-flex align-self-stretch flex-row justify-content-start pt-3" >
                            <div className="align-self-stretch" style={{width:'100%'}}>
                                <div class="form-check d-flex align-items-center">
                                    <div className="d-flex" style={{flex:1}}>

                                    <input class="form-check-input" type="checkbox" disabled={kyc == 'no'} value="" id="defaultCheck1"/>
                                    <label class="form-check-label" for="defaultCheck1">
                                        AADHAR Card
                                    </label>
                                    </div>
                                    <AttachBtnInline docsArr={aadhar} addDocsFn={addFlow2Docs} type='aadhar' disabled={kyc == 'no'}/>
                                </div>
                                <div class="form-check d-flex align-items-center">
                                   <div className="d-flex" style={{flex:1}}>
                                    <input class="form-check-input" type="checkbox" disabled={kyc == 'no'} value="" id="defaultCheck2" />
                                        <label class="form-check-label" for="defaultCheck2">
                                            PAN Card
                                        </label>
                                   </div>
                                   <AttachBtnInline  docsArr={pan} addDocsFn={addFlow2Docs} type='pan' disabled={kyc == 'no'}/>
                                </div>
                                <div class="form-check d-flex align-items-center">
                                    <div className="d-flex" style={{flex:1}}>
                                        <input class="form-check-input" type="checkbox" disabled={kyc == 'no'} value="" id="defaultCheck3" />
                                        <label class="form-check-label" for="defaultCheck3">
                                            GSTIN Details
                                        </label>
                                    </div>
                                    <AttachBtnInline  docsArr={gst} addDocsFn={addFlow2Docs} type='gst' disabled={kyc == 'no'}/>
                                </div>
                                <div class="form-check d-flex align-items-center">
                                    <div className="d-flex" style={{flex:1}}>
                                        <input class="form-check-input" type="checkbox" disabled={kyc == 'no'} value="" id="defaultCheck4" />
                                        <label class="form-check-label" for="defaultCheck4">
                                            ROC Documents
                                        </label>
                                    </div>
                                    <AttachBtnInline docsArr={roc} addDocsFn={addFlow2Docs} type="roc" disabled={kyc == 'no'} />
                                </div>
                                <div class="form-check d-flex align-items-center">
                                    <div className="d-flex" style={{flex:1}}>
                                        <input class="form-check-input" type="checkbox" disabled={kyc == 'no'} value="" id="defaultCheck5" />
                                        <label class="form-check-label" for="defaultCheck5">
                                            Bank Details
                                        </label>
                                    </div>
                                    <AttachBtnInline docsArr={bank} deleteDocsFn={deleteFlow2File} addDocsFn={addFlow2Docs} type="bank" disabled={kyc == 'no'}/>
                                </div>
                            </div>

                           
                           
                       </div>
                   </div>
                </div>
            </div>
        </div>
        
        </>
    )}
    

    
    const stepFour =() =>{
        return(
           
        <div className="modal-body text-center p-5">
            <h6 style={{color:'#473122'}}>Success</h6>

            <div style={{display:'flex',  justifyContent:'center', alignItems:'center'}}>
                <img src={success_img} style={{width:323, height:212}}/>
            </div>
            <div className="d-flex flex-column">
                <span className="py-2 align-self-center">Your record has been successfully created</span>
                <div className="p-3 align-self-center" style={{backgroundColor: colors.globalBG, fontSize:14, border:'1px dashed #eddfd5', fontWeight:'500', width:'70%'}}>
                    UIN: {uin}
                </div>
            </div> 
        </div>
        
        
    )
    }

    const renderSteps= () => {
        if(goldType === 1){
            switch(step){
                case 2:
                    return stepTwoFlow1()
                case 3:
                    return stepThreeFlow1()
                case 4:
                    return stepFour()
                default:
                    return null
            }
        }else{
            switch(step){
                case 2:
                    return stepTwoFlow2()
                case 3:
                    return stepThreeFlow2()
                case 4:
                    return stepFour()
                default:
                    return null
            }
        }
    }
    return(
        <div className="modal fade" id="exampleModalCenter" tabIndex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div className="modal-dialog modal-dialog-centered modal-lg" role="document">
                <div className="modal-content basic_modal">
                   {step > 1 && step < 4 && 
                   <div style={{position:'absolute', top:45, left:175, zIndex:2}}> 
                        <button type="button" className="btn" onClick={()=>moveBack()}><i className="fa fa-arrow-left" aria-hidden="true" style={{color:'#473122', fontSize:16, opacity:0.5}}></i></button>
                    </div>}
                    {
                        step ===1 ? stepOne() : renderSteps()
                    }
                <div style={{display:'flex', justifyContent:'flex-end', alignItems:'center'}}>
                    { <PrimaryButton label={`${step === 3? 'Done': step ===4? 'Okay Got It' :'Next'}`} disabled={isDisabled()} onClick={()=> moveSteps()} padding='11px 74px'/>}
                </div>
                
                </div>
            </div>
        </div>
    )
}