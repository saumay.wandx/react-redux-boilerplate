import React, {useState, useEffect} from 'react'
import './AddBatchModal.css'
import Modal from 'react-modal';
import { colors } from '../../constants/colors';
import PrimaryButton from '../../common/PrimaryButton/PrimaryButton';

import success_img from '../../assets/images/illustrations/success.png'


Modal.setAppElement(document.getElementById('root'))
export default function AddBatchModal(props) {

  class BarSchema{
    constructor(){
      this.id=Math.floor(Math.random(9)*100000000)
      this.barno=''
      this.weight=''
      this.ineness=''
    }
  }

  // console.log(props.activeItem)
    const [step, setStep] = useState(1)
    const [modalIsOpen,setIsOpen] = React.useState(false);
    const [batches, setBatches] = useState([{
      id: Math.floor(Math.random(9)*100000000),
      batchno: '',
      bars:[{id:Math.floor(Math.random(9)*100000000),barno:'',weight:'',fineness:''}]
  }])
    const [batchno,setBatchNo] = React.useState('');
    const [barno, setBarno] = useState('')
    const [barweight, setBarWeight] = useState('')
    const [barfine, setBarfine] = useState('')
   //const [bars, setBars] = useState([])
    const [vault, setVault] =useState(null)
    const [transport, setTransprt] =useState(null)
    const [bisStandard, setBisStandard]= useState(false)
    const [bisApproved, setBisApproved]= useState(false)


    useEffect(() => {
      // console.log(batchno.length)
      // console.log(batches.length)
      // console.log(batches.find(i=> i.batchno === batchno)?.bars?.length)
      // console.log(batchno.length < 1 && batches.length===0 && batches.find(i=> i.batchno === batchno)?.bars?.length == 0)
      // console.log(batchno.length < 1 && batches.length===0)
    }, [batchno])

  
    const moveSteps = async () => {
                switch(step){
                  case 1:
                      setStep(2)
                      break
                  case 2:
                      // setStep(3)
                      let newBatch = batches.map(batch=>{
                        let bars = batch.bars.filter(bar=>{ if(bar.barno != '' && bar.weight != '' && bar.fineness != ''){return bar} })
                        console.log(bars)
                        return {
                          ...batch,
                          bars
                        }
                      })
                     
                      let payload = {
                          uin:props.activeItem,
                          batch: newBatch
                      }
                       
                      // captureData(payload).then(res=>{ 
                      //     if(res){
                      //         console.log(res)
                      //         //setUIN(res.data._id)
                      //         setStep(3)
                      //     }
                      // })
                      break
                  case 3:
                      window.location.reload()
                      // window.location.href= '/records'
                      break
                  default:
                      return false
              }
  
      }
  
      const moveBack= () =>{
          setStep(step-1)
      }

      const isDisabled = () =>{
     
            switch(step){
                case 1:
                    return (batches.length===0)
                case 2:
                    return !(transport && vault)
                // case 3:
                //     return !(countryOrigin.trim().length>0 && shipperName.trim().length>0)
                case 3:
                    return false
                default:
                    return true
            }
            

    }

  
    function openModal() {
        setIsOpen(true);
      }
     
      function afterOpenModal() {
        // references are now sync'd and can be accessed.
        //subtitle.style.color = '#f00';
      }
     
      function closeModal(){
        setIsOpen(false);
        setBatchNo('')
      }

      const addBatch = ()=>{
          let payload={
              id: Math.floor(Math.random(9)*100000000),
              batchno: '',
              bars:[new BarSchema()]
          }
          let arr = batches
          setBatches(arr.concat(payload))
          setBarWeight('');
          setBarfine('')
          setBarno('')
          setBatchNo('')
          //setBars([])
      }

      const deleteBatch =(item)=> {
          console.log(item)
       let newArr= batches.filter(i=> i.id !== item.id)
       setBatches(newArr)
      }

      const addBars = (item) => {
            let payload = {id: Math.floor(Math.random(9)*100000000),fineness:'', weight:'', barno:''}
            // setBars(bars.concat([payload]))
            // setBarWeight('');
            // setBarfine('')
            // setBarno('')
            setBatches((currentBatches)=> currentBatches.map(batch=>{
              if(batch.id === item.id){
                console.log('batch', batch)
                let bars = batch.bars.concat([new BarSchema()])
                console.log(bars)
                  return{
                    ...batch,
                    bars
                  }
              }else return batch
            }))
      }
      const deleteBar = (bar, item) => {
        // let newArr= bars.filter(i=> i.id !== item.id)
        // setBars(newArr)
        setBatches((currentBatches)=> currentBatches.map(batch=>{
          if(batch.id === item.id){
            console.log('batch', batch)
            let bars = batch.bars.filter(i=> i.id !== bar.id)
            console.log(bars)
              return{
                ...batch,
                bars
              }
          }else return batch
        }))
      }

      const getBarsCount =()=>{
        let sum=0
         for(let i=0 ; i<batches.length ;i++){
            let bars = batches[i].bars.filter(i=>{
              if(i.weight.length>0 && i.barno.length>0 & i.fineness.length>0){
                return i
              }
            })
            sum += bars.length 
         }
         return sum
      }


      const changeBatchData = (e, target, item, subtarget=null, subtarget_id=null)=>{ 
        let newVal = e?.target?.value || '' ;
        setBatches((currentBatches)=> currentBatches.map(batch=> {
          if(batch.id === item.id){
             
             if(subtarget){
               
                let newBarsArr= batch.bars.map((bar, index)=>{
                  if(bar.id === subtarget_id){
                     return  {
                        ...bar,
                        [`${subtarget}`]: newVal
                      }
                  }else return bar
                })
                
                return{
                  ...batch,
                  bars:newBarsArr
                }

             }
             else if(target === 'bars' && subtarget === null){
               
              let newBars= batch.bars.push({id:Math.floor(Math.random(9)*100000000),barno:'',weight:'',fineness:''})
              console.log(newBars)
              let payload = {
                ...batch,
                [`${target}`]: newBars
              }
              console.log(payload)
              return payload
             }
             
             else{
              return {
                ...batch,
                [`${target}`]: newVal
              }
             }
          }
          else return  batch}))
      }
      

     const stepOne =() => {
        return(
          <div className="modal-body text-center pt-4">
              <h6 style={{color:'#473122'}}>Add Batch & Bar Information</h6>
              <div className="d-flex flex-column justify-content-start align-items-center" >
              
              <div className="form-group fuj_form_group pt-4 md-6 modalForm addBatch" style={{width:'80%'}}> 
               
               {batches?.map((item, index)=> 
               <div className="py-3" style={{width:'100%'}}>
                  <div className="d-flex flex-row align-items-start">
                    <input type="text" onChange={(e)=> changeBatchData(e,'batchno', item)}  placeholder="Batch Number" className="form-control mb-4 col-11" value={item.batchno} id="bacthno"/>
                     
                   {index != batches.length -1 && <button className="btn btn-light btn-sm mt-2" style={{background:'transparent', border:'none'}} onClick={(e)=>deleteBatch(item)}><i className="fa fa-times" aria-hidden="true" style={{ color: '#000', fontSize:15, opacity:0.5}}></i></button>}
                    </div>

                    <div className="p-3 d-flex flex-column align-self-stretch text-left" style={{borderRadius:14, border:'1px solid #eddfd5'}}>
                    <div style={{color:'#473122', fontSize:14}} className="py-2 px-3">Capture Bar Details</div>
                       {item?.bars?.map((bar,ind, arr)=>
                        <div key={bar.id.toString()} style={{position:'relative'}}> 
                        
                       <div className="d-flex flex-row align-items-center">
                           <div className="col-md-4">
                           <input type="text" onChange={(e)=> changeBatchData(e,'bars', item, 'barno', bar.id)} placeholder="Bar Number" className="form-control mb-4" value={bar.barno}   id="bacthno"/>
                           </div>
                           <div className="col-md-4">
                           <input type="text" onChange={(e)=> changeBatchData(e,'bars', item, 'weight', bar.id)} placeholder="Bar Weight" className="form-control mb-4" value={bar.weight}  id="bacthno"/>
                           </div>
                           <div className="col-md-4">
                           <input type="text" onChange={(e)=> changeBatchData(e,'bars', item, 'fineness', bar.id)} placeholder="Fineness" className="form-control mb-4" value={bar.fineness} id="bacthno"/>
                           </div>
                       </div>
                       {ind != arr.length-1 && <button className="btn btn-light btn-sm mt-2" style={{background:'transparent', border:'none', position:'absolute', right:-15, top:5}} onClick={(e)=>deleteBar(bar, item)}><i className="fa fa-times" aria-hidden="true" style={{ color: '#000', fontSize:15, opacity:0.5}}></i></button>}
                       </div>
                       )}
                       <AddButton label="Add Bar" onClick={()=> addBars(item)}/>  
                    </div>            
                </div>        
              )}


                {/* <div className="py-3">
                  <input type="text"  placeholder="Batch Number" className="form-control mb-4" value={batchno} onChange={(e)=> setBatchNo(e.target.value)} id="bacthno"/>

                  <div className="p-3 d-flex flex-column align-self-stretch text-left" style={{borderRadius:14, border:'1px solid #eddfd5'}}>
                    <div style={{color:'#473122', fontSize:14}} className="py-2 px-3">Capture Bar Details</div>
                        
                        */}
                    {/* {bars?.map((bar,ind)=>
                       <div style={{position:'relative'}}> 
                        
                       <div className="d-flex flex-row align-items-center">
                           <div className="col-md-4">
                           <input type="text" disabled placeholder="Bar Number" className="form-control mb-4" value={bar.barno}   id="bacthno"/>
                           </div>
                           <div className="col-md-4">
                           <input type="text" disabled placeholder="Bar Weight" className="form-control mb-4" value={bar.weight}  id="bacthno"/>
                           </div>
                           <div className="col-md-4">
                           <input type="text" disabled placeholder="Fineness" className="form-control mb-4" value={bar.fineness} id="bacthno"/>
                           </div>
                       </div>

                       <button className="btn btn-light btn-sm mt-2" style={{background:'transparent', border:'none', position:'absolute', right:-15, top:5}} onClick={(e)=>deleteBar(bar)}><i className="fa fa-times" aria-hidden="true" style={{ color: '#000', fontSize:15, opacity:0.5}}></i></button>
                       </div>
                       )} */}
                       {/* <div className="d-flex flex-row align-items-center">
                           <div className="col-md-4">
                           <input type="number"  placeholder="Bar Number" className="form-control mb-4" value={barno} onChange={(e)=> setBarno(e.target.value)}   id="bacthno"/>
                           </div>
                           <div className="col-md-4">
                           <input type="number"  placeholder="Bar Weight" className="form-control mb-4" value={barweight} onChange={(e)=> setBarWeight(e.target.value)}  id="bacthno"/>
                           </div>
                           <div className="col-md-4">
                           <input type="text"  placeholder="Fineness" className="form-control mb-4" value={barfine}  onChange={(e)=> setBarfine(e.target.value)}  id="bacthno"/>
                           </div>
                       </div> */}
                       
                       {/* <AddButton label="Add Bar" disabled={barno.length === 0 && barweight.length === 0 && barfine.length === 0} onClick={()=> addBars()}/>   */}
                    {/* </div> 
                </div>  */}

                <div style={{alignSelf:'flex-start', display:'flex'}}>
                <AddButton disabled={batchno.length < 1 && batches.length < 1} label="Add batch" onClick={()=> addBatch()}/>
                </div>
            </div>
          </div>
          
        </div>
        )
      }
      
     const stepTwo =() => {
        return(
           
              <div className="modal-body text-center pt-4">
              <h6 style={{color:'#473122'}}>Add Batch & Bar Information</h6>
              <div className="d-flex flex-column justify-content-start align-items-center" >
              
              <div className="form-group fuj_form_group pt-4 col-12 modalForm addBatch d-flex flex-column align-items-center " style={{width:'100%'}}> 
                    
              <div className="d-flex flex-row align-items-center align-self-stretch ">
                    <div className="col-md-6">
                      <div  className="form-control mb-4 yellowBGDiv d-flex flex-column align-self-start" >
                        <span style={{fontSize:10, alignSelf:'flex-start'}}>Total Batches</span>
                        <div className="yellowBGText" style={{fontSize:14, alignSelf:'flex-start'}}>{batches.length}</div>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div  className="form-control mb-4 yellowBGDiv d-flex flex-column align-self-start"  >
                        <span style={{fontSize:10, alignSelf:'flex-start'}}>Total Bars</span>
                        <div className="yellowBGText" style={{fontSize:14, alignSelf:'flex-start'}}>{getBarsCount()}</div>
                      </div>
                    </div>
                    
                </div>
              <div className="d-flex flex-row align-items-center align-self-stretch justify-content-between px-4">
                  <div class="form-check d-flex align-items-center justify-content-between">
                        <div className="d-flex" style={{flex:1}}>

                        <input class="form-check-input" type="checkbox"  id="defaultCheck1" checked={bisStandard} onChange={(e)=> setBisStandard(e.target.checked)}/>
                        <label class="form-check-label" for="defaultCheck1">
                        All bars adhere to BIS standard?
                        </label>
                        </div>
                    </div>
                    <div class="form-check d-flex align-items-center">
                        <div className="d-flex" style={{flex:1}}>
                        <input class="form-check-input" type="checkbox" checked={bisApproved} onChange={(e)=> setBisApproved(e.target.checked)} id="defaultCheck2" />
                            <label class="form-check-label" for="defaultCheck2">
                            BIS approved assay procedure followed
                            </label>
                        </div>
                    </div>
                </div>
            </div>
          </div>

          <h6 style={{color:'#473122',paddingTop:30}}>Storage & Transport Information</h6>

          <div className="d-flex flex-column justify-content-start align-items-center px-4" >
              
              <div className="form-group fuj_form_group pt-4 col-12 modalForm addBatch d-block " style={{width:'100%'}}> 
               
                <select class="custom-select yellowBGDiv my-2" style={{height:50}} onChange={(e)=>setVault(e.target.value)}>
                  <option selected>Select Vault Provide</option>
                  <option value="1">Example Vault</option>
                </select>
                
                <select class="custom-select yellowBGDiv my-2" style={{height:50}} onChange={(e)=>setTransprt(e.target.value)}>
                  <option selected>Select Transport Provider</option>
                  <option value="Brinks Transport">Brinks Transport</option>
                </select>
               
            </div>
          </div>
          
        </div>  
        
        )
      }
     const stepThree =() => {
        return(
           
          <div className="modal-body text-center pt-4">
            <h6 style={{color:'#473122'}}>Add Batch & Bar Information</h6>
              
            <div style={{display:'flex',  justifyContent:'center', alignItems:'center'}}>
                <img src={success_img} style={{width:323, height:212}}/>
            </div>
            <div className="d-flex flex-column">
                <span className="py-2 align-self-center">Successfully captured production information</span>
                
            </div> 
         

           
          
          </div>  
        
        )
      }

      const renderSteps = ()=>{
        switch(step){
          case 2:
            return stepTwo()
          case 3:
            return stepThree()
          default:
            return null
        }
      }

    return (
       <div>
        {/* <button >Open Modal</button> */}
        <button onClick={openModal} className="btn px-3 py-2" style={{background: colors.dullorange, color: '#fff', fontWeight:'500', fontSize:12, borderRadius: 8, boxShadow:'0 2px 10px 0 #eb902d'}}>Capture Details</button>
        <Modal
          isOpen={modalIsOpen}
          onAfterOpen={afterOpenModal}
          onRequestClose={closeModal}
          
          contentLabel="Example Modal"
          overlayClassName="AddBatchModalOverlay"
          className="AddBatchModal"
        >
 
        <div className="" style={{position:'relative'}}>
        {step ===2 && 
                   <div style={{position:'absolute', top:15, left:35, zIndex:2}}> 
                        <button type="button" className="btn" onClick={()=>moveBack()}><i className="fa fa-arrow-left" aria-hidden="true" style={{color:'#473122', fontSize:16, opacity:0.5}}></i></button>
                    </div>}
          {
              step ===1 ? stepOne() : renderSteps()
          }
          <div style={{display:'inline-block', float:'right', position:'sticky', bottom:0, right:0}}>
              { <PrimaryButton label={`${step === 2? 'Done': step ===3? 'Go to Records' :'Next'}`} disabled={isDisabled()} onClick={()=> moveSteps()} padding='11px 74px'/>}
          </div>
      </div>
          
      </Modal>
    </div>
    )
}

const customStyles = {
    content : {
      top                   : '50%',
      left                  : '50%',
      right                 : 'auto',
      bottom                : 'auto',
      marginRight           : '-50%',
      transform             : 'translate(-50%, -50%)',

    }
  };

  const AddButton =(props)=>{
    return(<div className="d-flex text-left" style={{flex:1}}>
        <button type="button" className="btn d-flex flex-row align-items-center" disabled={props.disabled} onClick={()=> props.onClick()}>
            <span style={{display:'flex',background: colors.dullorange, color: colors.browntext, padding:15, fontSize:10, justifyContent:'center', alignItems:'center', width:17, height:17, borderRadius:5, textAlign:'center', fontWeight:'500', boxShadow:'1px 2px 3px 0 #f1d3b2', marginRight:10}}>+</span>
            <span style={{color: colors.browntext, fontSize:12, fontWeight:'500' }}>{props.label}</span>
        </button>
    </div>)
    }