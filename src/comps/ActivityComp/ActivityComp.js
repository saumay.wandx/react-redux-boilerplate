import React from 'react'
import './ActivityComp.css'

export default function ActivityComp(props) {



    return (
        <div className="activity mb-4" style={{backgroundColor:'#fff', borderRadius:16, boxShadow:'0 4px 10px #eddfd5'}}>
            <div style={{flex:1, display:'flex', justifyContent:'flex-start', padding:15, fontSize:14}}>
                Activity Feed
            </div>

            <ul className="activities" style={{height: props.height ||280, overflow:'scroll'}}>
                {[1,2,2,2,2,2,2,2,2,2,2,2,2,,2,2,2,2,2,].map((item, index)=><Activity key={index.toString()}/>)}
            </ul>
        </div>
    )
}


const Activity = (props) => {
    return(
        <li>
            <div className="text-left" style={{display:'flex', flexDirection:'row', padding:'10px 10px'}}>
                <div style={{display:'flex', padding:'0px 5px'}}>
                    <div style={{padding:10, borderRadius:10, width:32, height:32, boxShadow:'0 4px 10px 0 #eddfd5', display:'flex', justifyContent:'center', alignItems:'center'}}>
                        <i className="fa fa-user" aria-hidden="true" style={{fontSize:21, color:'#eddfd5'}}></i>
                    </div>
                </div>

                <div style={{display:'flex', flexDirection:'column', padding:'0px 5px', justifyContent:'start'}}>

                    <div style={{fontSize:10, display:'flex', fontWeight:'500'}}>
                        Saumay Paul
                    </div>
                    <div style={{fontSize:12, lineHeight:1.5, display:'flex', paddingTop:5}}>
                        Bruce wayne is batman. He is the Dark Knight. Beware of batman. Maar dalega tereko.
                    </div>

                </div>
            </div>
        </li>
    )
}