import React, { useEffect, useState } from 'react'
import { colors } from '../../constants/colors'
// import Loader from '../../common/Loader/Loader'

export default function RecordTraceList(props) {

    const [list, setList] = useState([])
    useEffect(() => {
        setList([])
        setTimeout(() => {
            setList([{type:1},{type:2},{type:3},{type:4}])
        }, 500);
    }, [props.active._id])
    const listContent =(item)=>{
        switch(item.type){
            case 1:
                return(
                    <>
                    <span style={{fontSize:12, color: '#c09071'}}>Today</span>
                    <span style={{fontSize:14, color: colors.browntext}}>{item.type}</span>
                    </>
                )
            break
            case 2:
                return(
                    <>
                    <span style={{fontSize:12, color: '#c09071'}}>Next2</span>
                    <span style={{fontSize:14, color: colors.browntext}}>{item.type}</span>
                    </>
                )
            break
            case 3:
                return(
                    <>
                    <span style={{fontSize:12, color: '#c09071'}}>next3</span>
                    <span style={{fontSize:14, color: colors.browntext}}>{item.type}</span>
                    </>
                )
            break
            case 4:
                return(
                    <>
                    <span style={{fontSize:12, color: '#c09071'}}>next4</span>
                    <span style={{fontSize:14, color: colors.browntext}}>{item.type}</span>
                    </>
                )
            break
            default:
                return null
        }
    }

    const listRender = (item,index, arrLength)=> {
        return(
            <li className="list-group-item border border-white pt-0 position-relative pb-4">
                <div className="d-flex flex-column">
                   {listContent(item)}
                </div>
    
               {index != arrLength-1 && <div className="trace-line position-absolute" style={{top:25, left:0, bottom:0, width:2, background:'#c09071',}} />}
    
                <div className="trace-circle position-absolute" style={{background:'#c09071', width:20, height:20, border:' 5px solid #eddfd5' ,borderRadius:10, top:0, left:-9}}/>
    
            </li>
            )
    }
    return (
        <>
        {list.length<0 ? <ul className="col-12 text-left pl-0">
           
           {list.map((i, index, arr) => listRender(i,index, arr.length))}
             
        </ul> :  <div/>}
        
        </>
    )
}
