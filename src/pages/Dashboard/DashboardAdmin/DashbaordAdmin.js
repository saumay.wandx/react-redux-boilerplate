import React, { useState, useEffect } from 'react'
import './DashbaordAdmin.css'
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import SliderComp from '../../../common/SliderComp/SliderComp';
import ActivityComp from '../../../comps/ActivityComp/ActivityComp';
import Departments from '../../../comps/Departments/Departments';
import Table from '../../../common/Table/Table';
import CreateNewModal from '../../../comps/CreateNewModal/CreateNewModal';
import { getAllRecords } from '../../../utils/apiCalls';
import { useDispatch, useSelector } from 'react-redux';

export default function DashbaordAdmin() {

    // const records = useSelector(state=> state.recordsReducer.records)

    const tableHeads1 = ["UIN No","Date","Total Batches","Sampling Status","Validation Check","Current Status"];
    const [active, setActive]=useState(null)
    const [all, setAll] =useState([])

    const dispatch = useDispatch()

    useEffect(() => {
        console.log('getallrecords')
       // getallrecords()
    }, [])

    // const getallrecords = async ()=>{
    //     let res = await getAllRecords({});
    //     if(res?.data){
    //         //console.log(res)
    //         let records = res.data.goldDore.concat(res.data.goldScrap)
    //         setAll(records)
    //         dispatch(addRecords(records))
    //     }
    // }

    return (
        <div>
           

            <div className="dashboardContainer pb-4">
             
             <div className="px-4 pt-5 text-left">
                <h4>Overview</h4>
             </div>

               <div className="d-flex flex-lg-row flex-column">
                   <div className="col-12 col-lg-9 pl-4">
                        <SliderComp />
                        <div style={{backgroundColor:'#fff', borderRadius:16,boxShadow:'0 20px 40px 0 #eddfd5', overflow:'hidden'}}>
                           <div className="d-flex flex-column align-items-start" style={{padding:15, fontSize:18, fontWeight:'500'}}>
                                History

                            <div className="btn-group btn-group-toggle my-4" data-toggle="buttons">
                                <label className="btn filter_btns ">
                                    <input type="radio" name="options" id="option1" autoComplete="off"/> New Batches
                                </label>

                                <label className="btn filter_btns">
                                    <input type="radio" name="options" id="option2" autoComplete="off"/> Imported Batches
                                </label>

                                <label className="btn filter_btns">
                                    <input type="radio" name="options" id="option3" autoComplete="off"/> Sourced Batches
                                </label>
                            </div>
                            
                           <Table type={"all"} siteList={all} tableHeads={tableHeads1} handleClick= {()=> true} setItem={setActive} activeItem={active} onCaptureClick={()=> true}/>
                           </div>
                       </div>
                   </div>
                   <div className="d-flex flex-column col-lg-3 col-12 justify-content-around" style={{marginTop:30, flex:1}}>
                        <ActivityComp />
                        <Departments/>
                   </div>
               </div>
            </div>
            
        </div>
    )
}

