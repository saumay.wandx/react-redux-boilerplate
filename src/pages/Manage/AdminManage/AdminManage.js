import React, { useState } from 'react'
import { colors } from '../../../constants/colors'
import Departments from '../../../comps/Departments/Departments'
import ActivityComp from '../../../comps/ActivityComp/ActivityComp'

const AdminManage = () => {
    const [active, setActive]=useState(false)

    return (
        <div className="pb-4" style={{marginLeft:220}}>

            <div className="dashboardContainer">

                <div className="px-4 pt-5 text-left">
                    <h4>Manage</h4>
                </div>

                <div className="d-flex flex-lg-row flex-column">
                   <div className="col-12 col-lg-9 pl-4">
                        <div className="d-flex flex-column align-items-start" style={{padding:15, fontSize:18, fontWeight:'500'}}>
                              

                            <div className="btn-group btn-group-toggle my-4" data-toggle="buttons">
                                <label className="btn filter_btns ">
                                    <input type="radio" name="options" id="option1" autoComplete="off"/> New Batches
                                </label>

                                <label className="btn filter_btns">
                                    <input type="radio" name="options" id="option2" autoComplete="off"/> Imported Batches
                                </label>

                                <label className="btn filter_btns">
                                    <input type="radio" name="options" id="option3" autoComplete="off"/> Sourced Batches
                                </label>
                            </div>
                            
                           {/* <Table type={"all"} siteList={all} tableHeads={tableHeads1} handleClick= {()=> true} setItem={setActive} activeItem={active} onCaptureClick={()=> true}/> */}
                           </div>
                   </div>
                   
                   <div className="col-lg-3 col-12 d-flex" style={{flex:1,marginTop:30}}>
                        <ActivityComp height={400}/>
                   </div>
               </div>
            </div>
        </div>
    )
}

export default AdminManage
