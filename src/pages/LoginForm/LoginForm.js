import React, { useEffect, useState } from 'react'
import './LoginForm.css'; 
import { Link } from 'react-router-dom';
import Logo from '../../Layouts/Logo/Logo';
import { loginCall } from '../../utils/apiCalls';
import { setLocalItem, getLocalItem } from '../../constants/storage';
import { useDispatch } from 'react-redux';
import { loginAction } from '../../redux/actions/auth.action';
export default function LoginForm() {

    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [err, setErr] = useState(false)

    const dispatch = useDispatch();
    const onLoginPress = async ()=>{
        let payload = {};
        payload.username = username;
        payload.password = password;
        // let loginRes = await dispatch(loginAction(email, password))
        // let loginRes = await loginCall(payload)
        let loginRes = {status: true, data:{role:1}}
        console.log(loginRes)
        if(loginRes?.status){
            setLocalItem('loggedIn', loginRes.data)
            dispatch(loginAction(loginRes.data))
        }
    }
  
    function renderErr(){
        setTimeout(()=>{
                setErr(false)
        },3000)
            return(
                <div className="errMsg">
                <p>{err}</p>
            </div>
            )
    }

    return (
        <div className="container-fluid" > 
        <div className="pt-5">
            <Logo />    
        </div> 
        <div className="row justify-content-center align-items-center py-5" id="loginForm">
            <div className="col-lg-12 col-sm-8"> 
                <div className="row justify-content-center align-items-center " >
                    <div className="col-lg-5" > 
                        <div id="fuj-login-form" className="p-5">
                            <div className="form p-3 text-left">
                                    <h3>Sign In!</h3>
                                    <small >Sign into your account here</small>
                                <div className="form-group fuj_form_group pt-4"> 
                                   
                                    <input type="text"  placeholder="Work Email ID" className="form-control mb-4" onChange={(e)=>setUsername(e.target.value)} id="signInEmail"/>
                                    <input type="password" placeholder="Password" className="form-control mb-4" onChange={(e)=>setPassword(e.target.value)} id="signInPassword"/>
                                    
                                    <small id="formForgotSpan"><Link> Forgot Password?</Link></small>
                                </div>
                                {err? renderErr() :null}

                                 
                                <div className="form-group text-center pt-5">
                                    <button type="submit" className="btn my_cus_btn" onClick={(e)=> onLoginPress()}>Sign in</button>
                                </div>
                            </div>
                           
                        </div>

                        <div className="row justify-content-center align-items-start mt-3">
                                <div className="col-lg-12 text-center">
                                    <small style={{color:'#c09071', fontSize:14}}>Don't have an account? <Link to="/signup" >Sign Up</Link></small>
                                </div>
                            </div>

                    </div> 
                </div>
            </div>
        </div>  
    </div>
    )
}
