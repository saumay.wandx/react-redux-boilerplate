import React, { useState } from 'react'
import { colors } from '../../../constants/colors';
import RecordTraceList from '../../../comps/RecordTraceList/RecordTraceList';
import Table from '../../../common/Table/Table';

export default function RefinerRecords() {
    const tableHeads1 = ["UIN No","Date","Total Batches","Total Bars","Sampling Status","Validation Check","Current Status"];
    const [active, setActive]=useState(false)
     
    const [all, setAll] = useState([

        {
            _id: "UIN11BN79800",
            createdAt: "2019-12-04T00:00:00.000Z",
            totalBatches: 6,
            samplingStatus: 0, //not inititated
            validationCheck: 0, //pending
            currentStatus: "Production Complete",
        },
        {
            _id: "UIN12BN79800",
            createdAt: "2019-12-04T00:00:00.000Z",
            totalBatches: 8,
            samplingStatus: 0, //not inititated
            validationCheck: 1, //pass
            currentStatus: "Marking in production department",
        },
        {
            _id: "UIN13BN79800",
            createdAt: "2019-12-04T00:00:00.000Z",
            totalBatches: 4,
            samplingStatus: 1, //pending
            validationCheck: 1, //pass
            currentStatus: "Samples received by reference lab",
        },
        {
            _id: "UIN14BN79800",
            createdAt: "2019-12-04T00:00:00.000Z",
            totalBatches: 9,
            samplingStatus: 2, //complete
            validationCheck: 1, //pass
            currentStatus: "Samples received by",
            reciever:'BRINKS'
        },

    

    ])
    return (
        <div className="pb-4" style={{marginLeft:220}}>

            <div className="dashboardContainer">

                <div className="px-4 pt-5 text-left">
                    <h4>Records</h4>
                </div>

                <div className="px-4 d-flex flex-row">

                
                    <div className={`d-flex flex-column ${active ? 'col-7 mr-2' : 'col-12'}`} style={{backgroundColor:'#fff', borderRadius:16,boxShadow:'0 20px 40px 0 #eddfd5', overflow:'hidden' ,}}>
                           <div className="align-items-start" style={{flex:1, display:'flex', justifyContent:'flex-start', padding:15, fontSize:18, fontWeight:'500',flexDirection:'column' ,  width:'max-content'}}>

                            <div className="btn-group btn-group-toggle my-4" data-toggle="buttons">
                                <label className="btn filter_btns ">
                                    <input type="radio" name="options" id="option1" autoComplete="off"/> New Records
                                </label>

                                <label className="btn filter_btns">
                                    <input type="radio" name="options" id="option2" autoComplete="off"/> Imported Dore
                                </label>

                                <label className="btn filter_btns">
                                    <input type="radio" name="options" id="option3" autoComplete="off"/> Sourced Scraps
                                </label>
                            </div>
                            
                           <Table type={"records"} siteList={all} tableHeads={tableHeads1} handleClick= {()=> true} setItem={setActive} activeItem={active} onCaptureClick={()=> true}/>
                           </div>
                       </div> 

                    {active &&
                        <div className="d-flex flex-column col-5" style={{backgroundColor:'#fff', borderRadius:16,boxShadow:'0 20px 40px 0 #eddfd5', overflow:'hidden'}}>

                            <button className=" btn btn-sm btn-link position-absolute" style={{top:10 ,left:10}} onClick={e=> setActive(null)}><i class="fa fa-times" aria-hidden="true" style={{fontSize:14, color:colors.dullorange}}></i></button>

                             <div className="align-items-start" style={{flex:1, display:'flex', justifyContent:'flex-start', padding:15, fontSize:18, fontWeight:'500',flexDirection:'column'}}>

                            <div className="btn-group btn-group-toggle my-4" data-toggle="buttons">
                                <label className="btn filter_btns ">
                                    <input type="radio" name="options" id="option1" autoComplete="off"/> Journey
                                </label>

                                <label className="btn filter_btns">
                                    <input type="radio" name="options" id="option2" autoComplete="off"/> Supporting Documents
                                </label>
                            </div>
                            
                            <RecordTraceList active={active} />
                            
                           </div>
                        </div>
                    }
                    </div>
            </div>
        </div>
    )
}
